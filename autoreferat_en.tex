\documentclass[journal=ancham, manuscript=article, layout=onecolumn]{achemso}
% remove the email address field
\setkeys{acs}{email=false}
% remove star next to the author name
\makeatletter
\def\acs@author@fnsymbol#1{}
\makeatother

\usepackage{my_style}
\graphicspath{{../PHD/img/}}

\author{Mateusz Krzysztof Łącki}
\email{mateusz.lacki@biol.uw.edu.pl}
\affiliation{Department of Mathematics, Informatics, and Mechanics, University of Warsaw}
\title{Computational and Statistical Methods for Mass Spectrometry Data Analysis\\
{\normalsize summary of the personal accomplishments}}

\newtheorem{defi}{Definicja}[section]

\begin{document}
\maketitle

Mass Spectrometry is a subfield of the Analytical Chemistry that studies and develops instruments useful for analyzing the molecular content of samples.
The instruments, that are called mass spectrometers, have been developed by Joseph J. Thomson just before the First World War. The output of a mass spectrometer -- a \textit{mass spectrum} -- is a histogram: one the x-axis it reports the observed mass-to-charge ratios, and on the y-axis -- it reports the intensities within these groups. 
The intensities are usually assumed to be proportional to the number of ions. 
Below, we show how a mass spectrum might look like.
\begin{figure}[h]
  \centering
  \includegraphics[width=.9\textwidth]{spectrum.png}
\end{figure}

\begin{table}
\caption[Masses and Frequencies of isotopes of elements that make up proteins.]{Masses and Frequencies of isotopes of elements that make up proteins.\\Source: \citet{IUPAC}.}\label{IUPAC}
\centering
\begin{tabular}{lcc}
Isotope & Mass & Abundance \\
  \hline
  \ce{^1 H}           & 1.0078  & 0.9999 \\
  \ce{^2 H} (\ce{D})  & 2.0141  & 0.0001 \\
  \ce{^{12} C}        & 12.0000 & 0.9892 \\
  \ce{^{13} C}        & 13.0034 & 0.0108 \\
  \ce{^{14} N}        & 14.0031 & 0.9964 \\
  \ce{^{15} N}        & 15.0001 & 0.0036 \\
  \ce{^{16} O}        & 15.9949 & 0.9976 \\
  \ce{^{17} O}        & 16.9991 & 0.0004 \\
  \ce{^{18} O}        & 17.9992 & 0.0021 \\
  \ce{^{32} S}        & 31.9721 & 0.9499 \\
  \ce{^{33} S}        & 32.9715 & 0.0075 \\
  \ce{^{34} S}        & 33.9679 & 0.0425 \\
  \ce{^{36} S}        & 35.9671 & 0.0001 \\\hline
\end{tabular}
\end{table}
Consider a chemical compound with a fixed formula, such as the human insuline, \ce{C520H817N139O147S8}. What kind of signal can it produce in a mass spectrum? To answer this question, we have to consider several factors. 
First of all, all atoms in the above formula can assume one of the multiple isotopic variants, from a set that is different for each element.
Table~\ref{IUPAC} summarizes these possible variants. 
The reported abundances reported there are the marginal probabilities of the isotopic variants: they describe the possible isotopic variant of only one atom.
By far the easiest way to construct a joint probability measure out of these marginals is to assume that isotopic variants of different atoms are mutually independent. 
In certain situations, such as the isotopic labeling, one would certainly have to modify that model, as these could introduce some non-trivial dependence between the isotopic variants.

Mass spectrometer does not distinguish compounds with the same number of isotopic variants.
For instance, if one of the 817 hydrogen atoms of human insulin is deuterium, then, based on mass spectrum alone, there is no way of telling which particular atom was the heavier one.
The observed signal depends only upon counts of different isotopic variants. 
Similarly to how chemical formulas such as \ce{C_c H_h N_n O_o S_s} abstract from spatial composition, we may introduce a more detailed description of the isotopic content of a molecule, such as 
$${\tt iso} = \ce{^{12}C_{c_0} ^{13}C_{c_1} ^{1}H_{h_0} ^{2}H_{h_1} ^{14}N_{n_0} ^{15}N_{n_1} ^{16}O_{o_0} ^{17}O_{o_1} ^{18}O_{o_2} ^{32}S_{s_0} ^{33}S_{s_1} ^{34}S_{s_2} ^{36}S_{s_3} }.$$
Above, $\text{c}_\text{0}$ stands for the number of \ce{^{12}C} isotopes within the molecule, $\text{c}_\text{1}$ -- number of \ce{^{13}C} isotopes, and so on. This is essentially what we call an \textit{isotopologue}.
This definition coincides with that provided by the International Union of Pure and Applied Chemistry \citep{IUPACisotopologue}.
The independence assumptions suggest that the probability of observing an isotopologue is that of a product of multinomial distributions, each for one element, or
\begin{gather*}
p_{\tt iso} = \binom{c}{c_0, c_1} \mathbb{P}\big(^{12}\text{C}\big)^{c_0} \mathbb{P}\big(^{13}\text{C}\big)^{c_1}
\binom{h}{h_0, h_1} \mathbb{P}\big(^{1}\text{H}\big)^{h_0} \mathbb{P}\big(\text{D}\big)^{h_1}
\binom{n}{n_0, n_1} \mathbb{P}\big(^{14}\text{N}\big)^{n_0} \mathbb{P}\big(^{15}\text{N}\big)^{n_1}\\ 
\times \binom{o}{o_0, o_1, o_2} \mathbb{P}\big(^{16}\text{O}\big)^{o_0} \mathbb{P}\big(^{17}\text{O}\big)^{o_1} \mathbb{P}\big(^{18}\text{O}\big)^{o_2}
\binom{o}{o_0, o_1, o_2} \mathbb{P}\big(^{32}\text{S}\big)^{s_0} \mathbb{P}\big(^{33}\text{S}\big)^{s_1} \mathbb{P}\big(^{34}\text{S}\big)^{s_2} \mathbb{P}\big(^{36}\text{S}\big)^{s_3}. 
\end{gather*}
The mass of ${\tt iso}$ is given by mutliplying counts of different isotopes times their masses (Table~\ref{IUPAC}), $m_{\tt iso} = m(^{12}\text{C}) c_0 + m(^{13}\text{C}) c_1 + \dots + m(^{36}\text{S}) s_3$.
The set of all pairs $(p_{\tt iso}, m_{\tt iso})$ that corresponds to one chemical formula \ce{C_c H_h N_n O_o S_s} makes up the \textit{isotopic fine structure}. 
The isotopic fine structure is typically used directly to model the signal in the instrument. 

\subsection*{Isotopic Calculations}

In Chapter 1 of my dissertation, I describe an efficient and elegant way to quickly generate high coverage subsets of the isotopic fine structure.
What is understood by a high coverage set, is a set of isotopologues with joint probability not smaller than some threshold $P$.
We call that set an optimal $P$-set.
The algorithm that retrieves the elements of the optimal $P$-set relies on two crucial properties of the multinomial distribution: its  \textit{concentration of measure} \citep{Talagrand1996} and unimodality.
Measure concentration assures that a relatively small number of configurations carry most of that distribution's probability mass.
Unimodality, defined in this context in terms of the connectedness of the set of local maxima, assures that it is easy to visit all configurations in a descending order.
These features also apply to the product of the multinomial distributions, which is precisely the probability measure that appears in the context of the isotopic calculations.

\begin{figure}[t]
    \centering
    \includegraphics[width=\linewidth]{cltSimplex.png}
    \caption[The quality of the Gaussian approximation to the optimal $P$-set for a toy example one element compound with two isotopes.]{The quality of the Gaussian approximation to the optimal $P$-set for a toy example one element compound with two isotopes. As predicted by the \textit{Central Limit Theorem}, the shape of the optimal $P$-set for a one element compound can be well approximated by an ellipsoid defined by the mean and covariance matrix of the multinomial distribution. The simplices are normalized to the number of atoms of the toy compound. Notice sublinear growth of the volume of the ellipse: according to approximations, its area should behave approximately like a square root of the number of atoms $n$.}\label{fig::clt on simplex}
\end{figure}

What is more, an algorithm that uses a simple FIFO queue is shown to efficiently perform the calculations. 
In particular, we show that the generation of consecutive peaks in an optimal $P$-set requires a linear number of calculations with respect to its size, which is optimal.
To establish the time complexity, we make use of the \textit{Central Limit Theorem} that we apply to the multinomial distribution, see Figure~\ref{fig::clt on simplex}.
We show, that the number of isotopologues inside the optimal $P$-set can be approximated well by formula
\begin{equation*}
    M = \frac{q_{\chi^2(k)}(p)^\frac{k}{2}}{C} \frac{\pi^{k/2}}{\Gamma(k/2+1)}\sqrt{ \prod_{e\in\mathcal{E}} \Big(n_e^{i_e - 1} \prod_{j=0}^{i_e-1} \tilde{d}_{ej} } \Big).
\end{equation*}
In particular, from the above fact one can deduce that by applying the joint probability thresholding we obtain a set of configuration that is of order of a square root of the total number of configurations that make up the isotopic distribution.
In most cases, this is why the calculations can be carried out, even for compounds as big as titin, \ce{C169719H270464N45688O52237S911}\footnote{Of course, we bring up this example only to illustrate the algorithms capabilities. It would not make too much sense to introduce to the instrument a compound that big.}.
What is more, the implementation of the algorithm significantly outperforms other existing isotopic calculators.

The applications of the ideas that resulted in this algorithm go beyond mass spectrometry, and their use is now investigated in statistics and stochastic simulation.


\subsection*{Deconvolution of Mass Spectra and Ion Statistics}

The results of the \IsoSpeC only describe the isotopic variants attainable by only one molecule.
Nonetheless, if one assumes that different ions appear independently in the instrument, then that distribution is directly proportional to the expected shape of the isotopic distribution. 
Therefore, it can be directly applied to the problem of establishing the presence of a signal coming from one source of ions, via non-negative regression schemes similar to that proposed by \citet{slawski2012isotope}.
We further generalize that idea in a project called \MassTodon, described in the next section.

In reality, the intensity observed in a mass spectrometer is a function of a high number of ions.
It has been long since established that the distribution of the number of ions should follow a Poisson distribution~\citep{Ipsen2012,Ipsen2015}.
The theoretical argument behind this is as simple: the number of ions that actually make it to the detector is very limited.
Assume that ions move independently throughout the instrument and that each one has some chance of reaching the detector. 
Then, the number of ions that made it to the detector can be directly modeled by a binomial model.
That distribution is well approximated by the Poisson distribution if the probability of the success, i.e. \textit{reaching the detector} is very small.
That is why the Poisson distribution is also sometimes referred to as the \textit{distribution of rare events}.
Chapter~4 describes our attempts to merge the concepts of the isotopic distribution with the Poissonian ion statistics into one tool that we call \MassOn.

\MassON tackles two important problems in mass spectrometry signal processing: 
\begin{enumerate}
    \item the deconvolution of a compound signal
    \item the estimation of the number of observed ions
\end{enumerate}
The nature of that first problem lies in the limited capability of a mass spectrometer to resolve close mass-to-charge ratios of different molecular species.
In particular, it can very well happen that isotopologues of different molecular species can contribute to the height of the same peak.
This idea is sketched in Figure~\ref{fig::ball spectrum}.
\begin{figure}[th]
    \centering
    \includegraphics[width=.8\linewidth]{ball_spectrum}
    \caption[Idealized convolution]{Schematic representation of the convolution of two isotopic distributions. Each ball represents one ion, either of kind A or kind B. The above pattern is typically found in problems where two formulas differ by exactly one hydrogen atom, as that difference shifts the spectrum by around 1 dalton. Also, if the $\frac{m}{z}$ is the ratio of the lightest isotope, then other isotopologues tend to cluster around $\frac{m+k}{z}$, where $k \in \mathbb{N}$, as the whole molecule can contain $k$ additional protons distributed among different atomic nuclei, and that additional proton mostly weights the same. Small differences, called the Kendrick mass defect, can be attributed to different energies required to hold the nucleus together \citep{Pourshahian2017}.}\label{fig::ball spectrum}
\end{figure}

The second problem stems from the fact that most of the instruments record the ion current that is deemed proportional to the number of passing ions, at least within their trusted dynamic range. % TODO: ADD REFERENCE. 
The problem of estimating the above proportionality factor is of great relevance, as it appears in most of the expressions involving the standard deviations of statistics derived from the theoretical mass spectrum. 
In particular, if one assumes that the recorded peak height indeed results from an independent motion of ions close to the detector, then the standard deviation of that peak is a function of the square root of the overall number of ions. 
Both Chapters~2 and 3 describe other important statistics that rely on the specification of the recorded number of ions.
\MassON tackles both these problems in a fully Bayesian setting relying on a \textit{data augmentated} Gibbs sampling scheme.

In that model, we treat molecular species as conditionally independent Poisson processes that populate the mass over charge half-line.
Every process can be assigned an intensity measure $\mu_m$, that relates the isotopic distribution to the mass-over-charge values,
\begin{equation}
    \mu_m (A | \Lambda_m) = \Lambda_m \sum_{w = 1}^W p_{mw} \mathtt{G}(A|{\tt MZ}_{mw}).
\end{equation}
Above, $\Lambda_m$ is the total intensity of the species $m$, $p_{mw}$ is the probability of observing an ion in the $m^\text{th}$ mass bin, centered at mass-to-charge ratio ${\tt MZ}_{mw}$.
The above processes are conditionally independent given the values of intensities $\Lambda_m$ and the marginal frequencies of isotopes, that are used to calculate $p_{mw}$.

These intensity measures describe, on average, the ionic content of the mass spectrum at different mass ranges.
These numbers are not directly observed in the mass spectrum.
We assume that the observed peak intensity $I^A$ in a mass-to-charge range $A = [a_\text{min}, a_\text{max}]$ is a linear function of the real number of ions, $I^A = C \times N^A$.
The model, apart from deconvoluting the isotopic distributions, can also find the \textit{a posteriori} distribution of the \textit{ion-intensity} exchange rate $C$.

\subsection*{Understanding Reaction Pathways}

Another limitation of any mass spectrometer is that substances with the same chemical formula and different structures cannot be told apart.
In particular, this is the case of two post-translationally modified proteins that have the same modification that could be found on more than one residue. 
The spatial positioning of a modification is critical for the folding of the protein, and thus -- its function.
To position a PTM, one has to use more specific techniques, fragmentation being one of them. 
Ions can be fragmented either outside the instrument, via proteolytic digestion, or inside the instrument. 
Two prominent ways of inducing fragmentation inside the instrument are the Collisional Induced Dissociation (CID) and the Electron Transfer Dissociation (ETD).
The first one consists in heating up the sample cations by exposing them to collisions with some inert gas. 
This method produces more noisy spectra, as different parts of the molecule detach due to their increased internal motion.
ETD is much more subtle technique: it consists of an ion-ion reaction between the sample cations and anions, each carrying a radical -- an electron in a higher energy state.
The meeting between these ions is deemed to result in four possible outcomes:
\begin{itemize}
  \item the transfer of electron from the anion to the cation resulting in the dissociation of the cation -- the proper ETD
  \item the transfer of electron that does not result in any dissociation -- ETnoD
  \item the ETD dissociation followed by a subsequent hydrogen transport -- HTR
  \item the proton transfer reaction -- PTR
\end{itemize}
Table~\ref{chemical_reactions} shows the details of the considered reactions.

\begin{table*}[b]
\centering
\begin{tabular}{rlcl}
    \textbf{PTR}    &\ce{[M + nH]^{n+} + A^{.-}}    &\ce{->}& \ce{[M + (n-1) H]^{(n-1)+} + AH}  \\
    \textbf{ETnoD}  &\ce{[M + nH]^{n+} + A^{.-}}    &\ce{->}& \ce{[M + nH]^{(n-1)+.} + A}      \\
    \textbf{ETD}    &\ce{[M + nH]^{n+} + A^{.-}}    &\ce{->}& \ce{[c + xH]^{x+} + [z + (n - x)H]^{(n-x-1)+.} + A}\\
    \textbf{HTR}    &\ce{[c + xH]^{x+}}             &\ce{->}& \ce{[c + (x - 1)H]^{x+}}\\
                    &\ce{[z + (n - x)H]^{(n-x-1)+}}     &\ce{->}& \ce{[z + (n - x + 1)H]^{(n-x-1)+}}
\end{tabular}
\caption[Considered chemical reactions by MassTodon.]{Considered chemical reactions. \ce{M} stands for either a precursor ion or a fragment ion. The HTR reaction can happen only after ETD and consists in the transfer of a hydrogen atom from the $c$ to the $z$ fragment.}\label{chemical_reactions}
\end{table*}


To study the products of these fragmentation, we devised an approach named \MassTodon.
Chapter~2 provides a detailed explanation of the approach we take to study electron transfer driven reactions.
The presented workflow can find the products of these reactions in the spectrum.
In order to do this, \MassTodon:
\begin{enumerate}
    \item generates the formulas of all possible reaction products
    \item investigates their presence in mass spectrum\label{point::investigation}
    \item deconvolutes different isotopic distributions using constrained quadratic programming
    \item pairs the intensities of the matching $c$ and $z$ ions in order to estimate the fragmentation probabilities and the branching ratio
\end{enumerate}
While performing point \ref{point::investigation} in the above listing, \MassTodoN applies \IsoSpeC to generate the expected shapes of the signal.
These are also used for deconvolution. 
To efficiently perform all the calculations, \MassTodoN relies on the concept of the \textit{Deconvolution Graph}.
The above graph representation offers the possibility to naturally parallelize the deconvolution calculations, that can be performed independently on different connected components.
Figure~\ref{fig::deconvolution_principles} shows how to relate the \textit{deconvolution graph} to the observed mass spectrum. 

\begin{figure}[t]
    \centering
    \includegraphics[width=.7\linewidth]{deconvolution_graph6}
    \caption{A connected component of the \textit{deconvolution graph}. In the above problem two molecular species, $M_A$ and $M_B$, can potentially generate the pink peaks of the experimental spectrum. Each species corresponds, in this simplified case, to three different isotopologue peaks. The proportions between these peaks, $p_{A_0} \div p_{A_1} \div p_{A_2}$ and $p_{B_0} \div p_{B_1} \div p_{B_2}$, are used to deconvolute the signal.
    The task of \MassTodoN is to estimate the total intensities of $M_A$ and $M_B$, denoted above by $\alpha_A$ and $\alpha_B$ respectively.}\label{fig::deconvolution_principles}
\end{figure}

\MassTodoN outputs the estimates of joint intensities of each chemical formula it found from the set of potential reaction products and substrates.
It can also estimate the probabilities with which different reactions occurred in one experiment.
This simplifies the comparison of different mass spectra, offering a possibility to better study the influence of different instrumental settings upon the sample; finally, it also simplifies the comparison of different instruments.
In particular, \MassTodoN has been already used to estimate the branching ratio (the odds ratio between ETnoD and PTR) that has been found to correlate well with the collision cross-section of proteins \citep{lermyte2017conformational}.

\subsection*{Unveiling Reaction Kinetics}

With the estimates of the intensity of particular molecular species at hand, one can pose more specific questions on the nature of the chemical process that could generate them.
A natural approach in this context is to apply the mathematical apparatus provided by the theory of reaction kinetics.
In particular, we adapted an approach based on a dynamic stochastic Petri net proposed by \citet{Gambin2010}, schematically shown in Figure~\ref{img::petrinet}.

\begin{figure}[t]
    \centering
    \includegraphics[width=.7\linewidth]{reaction_graph}
    \caption[A model of the ETD reaction.]{A model of the ETD reaction. (a) A fragment of the reaction graph for a triply charged precursor. The \textit{molecular species} are depicted in pale grey and the \textit{reactions} in dark grey. The  skull represents the \textit{cemetery}. The reaction graph serves as a board for \textit{tokens} that represent the numbers of molecules of a given species, depicted as black circles. Only one ETD transition has been shown for clarity of the image. (b) During each reaction, a token disappears on the substrate side and product tokens appear: one in the case of ETnoD and PTR, two in the case of ETD).}\label{img::petrinet}
\end{figure}

The model offers a parametrization of the process in terms of a set of reaction rates, specific for different reactions.
Due to the nature of the process, the considered net does not have any self-loops, which significantly simplifies the dynamics of the process.
In particular, one can derive recursive formulas for the average amounts of ions at each possible \textit{place} in the net, i.e. of each molecular species that can be observed during the reactions.
These formulas depend on the considered parameters.
By applying a gradient-free optimization algorithm, we try to find parameters that minimize the distance from the estimates of the total intensities of different molecular species that appear in the Petri net.

The developed tool, called \ETDetective, is integrated with \MassTodon.
The model provides a much more comprehensive way to study the outcomes of the electron-driven reactions, providing direct access to reaction rates that are easily interpretable by the chemists.
Chapter~3 describes in detail the approach we follow to perform these calculations.


\ornamentheader{Publications in the field of Mass Spectrometry}

\begin{itemize}
  \item Lermyte, F., Łącki, M. K., Valkenborg, D., Gambin, A., \& Sobott, F. (2017). Conformational space and stability of ETD charge reduction products of ubiquitin. Journal of The American Society for Mass Spectrometry, 28(1), 69-76.
  \item  Lermyte, F., Łącki, M. K., Valkenborg, D., Baggerman, G., Gambin, A., \& Sobott, F. (2015). Understanding reaction pathways in top-down ETD by dissecting isotope distributions: A mammoth task. International Journal of Mass Spectrometry, 390, 146-154. 
  \item Ciach, M. A., Łącki, M. K., Miasojedow, B., Lermyte, F., Valkenborg, D., Sobott, F., \& Gambin, A. (2017, May). Estimation of Rates of Reactions Triggered by Electron Transfer in Top-Down Mass Spectrometry. In International Symposium on Bioinformatics Research and Applications (pp. 96-107). Springer, Cham.
  \item Łącki, M. K., Lermyte, F., Miasojedow, B., Startek, M., Sobott, F., ... \& Gambin, A. (2017). Assigning peaks and modeling ETD in top-down mass spectrometry. arXiv preprint arXiv:1708.00234.
  \item Łącki, M. K., Startek, M., Valkenborg, D., \& Gambin, A. (2017). IsoSpec: Hyperfast Fine Structure Calculator. Analytical Chemistry, 89(6), 3272-3277.
\end{itemize}



\bibliography{bib/my_works,bib/algorithmics,bib/probability,bib/frederik,bib/spectrometry,bib/statistics,bib/ciach}

\end{document}
