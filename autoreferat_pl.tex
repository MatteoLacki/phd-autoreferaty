\documentclass[journal=ancham, manuscript=article, layout=onecolumn]{achemso}
\usepackage{my_style}
\usepackage{polyglossia}
\usepackage{booktabs}

% remove the email address field
\setkeys{acs}{email=false}
% remove star next to the author name
\makeatletter
\def\acs@author@fnsymbol#1{}
\makeatother

\setdefaultlanguage{polish}

\graphicspath{{../PHD/img/}}



\author{{\Futura Mateusz Krzysztof Łącki}}
\email{mateusz.lacki@biol.uw.edu.pl}
\affiliation{{\Futura Wydział Matematyki, Informatyki i Mechaniki Uniwersytetu Warszawskiego}}
\title{{\Futura\Large Metody obliczeniowe i statystyczne analizy danych ze spektrometrów masowych}\\
{\normalsize {\Futura autoreferat}}}

\newtheorem{defi}{Definicja}[section]

\begin{document}
\maketitle
Spektrometria mas to pod dziedzina chemii analitycznej zajmująca się rozwojem instrumentów służących określaniu molekularnej zawartości próbek.
Początków tej dziedziny należy upatrywać w pracach Josepha J. Thomsona z lat nastych XX wieku. 
Prace te dotyczyły istnienia izotopów pierwiastków występujących w przyrodzie\citep{thomson1913bakerian}.
Podstawowym narzędziem tej gałęzi nauki jest spektrometr masowy.
Wyniki przeprowadzonej przez instrument analizy zwyczajowo przedstawia się w postaci histogramu nazywanego \textit{widmem masowym}, jak na Rysunku~\ref{rys::spektrum substancji P}. 
Na osi odciętych odznaczane są stosunki masy do ładunku zaobserwowanych w czasie pomiaru jonów, natomiast na osi rzędnych -- intensywności zgrupowań jonów.
Rejestrowane intensywności, przedstawiane w formie słupków, uznawane są za proporcjonalne do liczby jonów tworzących sygnał.

\begin{figure}[h]
  \centering
  \includegraphics[width=.9\textwidth]{substanceP}
  \caption{Przykładowe widmo masowe pozyskane dla oczyszczonej próbki składającej się głównie z Substancji P, drobnego neuropeptydu.}\label{rys::spektrum substancji P}
\end{figure}
\begin{table}[t]
\caption{Masy oraz częstości występowania stabilnych izotopów pierwiastków występujących w białkach.\\Źródło: \citet{IUPAC}.}\label{IUPAC}
\centering
\begin{tabular}{lcc}
{\Futura Izotop}      & {\Futura Masa}    & {\Futura Częstość} \\
  \toprule
  \ce{^1 H}           & 1.0078  & 0.9999 \\
  \ce{^2 H} (\ce{D})  & 2.0141  & 0.0001 \\\midrule
  \ce{^{12} C}        & 12.0000 & 0.9892 \\
  \ce{^{13} C}        & 13.0034 & 0.0108 \\\midrule
  \ce{^{14} N}        & 14.0031 & 0.9964 \\
  \ce{^{15} N}        & 15.0001 & 0.0036 \\\midrule
  \ce{^{16} O}        & 15.9949 & 0.9976 \\
  \ce{^{17} O}        & 16.9991 & 0.0004 \\
  \ce{^{18} O}        & 17.9992 & 0.0021 \\\midrule
  \ce{^{32} S}        & 31.9721 & 0.9499 \\
  \ce{^{33} S}        & 32.9715 & 0.0075 \\
  \ce{^{34} S}        & 33.9679 & 0.0425 \\
  \ce{^{36} S}        & 35.9671 & 0.0001 \\\bottomrule
\end{tabular}
\end{table}

Aby unaocznić skomplikowanie zagadnienia analizy widma masowego, rozważmy, dla przykładu, cząsteczkę insuliny występującą w człowieku.
Jej wzór sumaryczny to \ce{C520H817N139O147S8}. 
Wydawać by się mogło, że starczy dodać do siebie masy tych wszystkich atomów, aby odnaleźć w widmie odpowiadający danej cząsteczce sygnał.
Niestety, sytuację znamienicie komplikuje istnienie izotopów i to tym bardziej, im dana cząsteczka jest większa.
Różna liczba neutronów w jądrach atomowych skutkuje zmianami w masach pierwiastków.
Napotkanie danego izotopu w przyrodzie jest też w dużej mierze zjawiskiem losowym. 
Nie oznacza to jednak, że nieprzewidywalnym: zwłaszcza wieksze zgromadzenia cząsteczek rządzą się wieloma dobrze zbadanymi prawidłowościami.
Międzynarodowa Unia Chemii Czystej i Stosowanej (IUPAC) prowadzi ciągłe badania nad frekwencjami różnych wariantów izotopowych w przyrodzie.
Tabela~\ref{IUPAC} przedstawia wyniki ich pomiarów na rok 2014 dla pierwiastków naturalnie występujących w białkach.

Z perspektywy modelowania statystycznego wyniki zebrane w Tabeli~\ref{IUPAC} reprezentują prawdopodobieństwa wariantów izotopowych dla poszczególnych cząsteczek.
Najłatwiejszym sposobem na uogólnienie tego prostego modelu na wiele atomów jest przyjęcie, że ich warianty izotopowe są wzajemnie niezależne.
Taki też model zaproponowano jeszcze w latach `60-ych\citep{beynon1960mass}.
W wyjątkowych sytuacjach, jak na przykład przy znakowaniu izotopowym, należałoby zmodyfikować ów model; w ogólności, niemniej, trudno wskazać jakiś teoretyczny mechanizm, który mógłby skutkować znaczącym odstępstwem od postulowanej niezależności wariantów izotopowych.
Również wyniki eksperymentalne zdają się potwierdzać słuszność tego założenia.

Spektrometr masowy nie jest w stanie rozróżnić położenia wariantów izotopowych w ramach jednej cząsteczki.
Przykładowo: jeśli wśród 817 atomów wodoru ludzkiej insuliny jest jeden deuter, nie bylibyśmy w stanie za pomocą spektrometru masowego określić dokładnej jego pozycji.
Obserwowane widmo zależy wyłącznie od liczb atomów przyjmujących dane warianty izotopowe.
Naturalne jest zatem wprowadzenie pełniejszego opisu cząsteczki od prostego wzoru sumarycznego \ce{C_c H_h N_n O_o S_s}, tak, aby uściślał on zawartość izotopową danej cząsteczki.
Dla białek taki zapis wygląda następująco:
$${\tt izo} = \ce{^{12}C_{c_0} ^{13}C_{c_1} ^{1}H_{h_0} ^{2}H_{h_1} ^{14}N_{n_0} ^{15}N_{n_1} ^{16}O_{o_0} ^{17}O_{o_1} ^{18}O_{o_2} ^{32}S_{s_0} ^{33}S_{s_1} ^{34}S_{s_2} ^{36}S_{s_3} },$$
gdzie $\text{c}_\text{0}$ oznacza liczbę atomów węgla \ce{^{12}C}, $\text{c}_\text{1}$ -- liczbą atomów węgla \ce{^{13}C}, itd. 
Tak rozumiany napis będziemy nazywać \textit{izotopologiem} cząsteczki.
Definicja ta bliska jest definicji dostarczonej przez IUPAC\citep{IUPACisotopologue}.
Z założenia o niezależności wariantów izotopowych wynika, że prawdopodobieństwo wystąpienia izotopologa to produkt rozkładów wielomianowych, wyznaczony wzorem
\begin{gather*}
p_{\tt izo} = \binom{c}{c_0, c_1} \mathbb{P}\big(^{12}\text{C}\big)^{c_0} \mathbb{P}\big(^{13}\text{C}\big)^{c_1}
\binom{h}{h_0, h_1} \mathbb{P}\big(^{1}\text{H}\big)^{h_0} \mathbb{P}\big(\text{D}\big)^{h_1}
\binom{n}{n_0, n_1} \mathbb{P}\big(^{14}\text{N}\big)^{n_0} \mathbb{P}\big(^{15}\text{N}\big)^{n_1}\\ 
\times \binom{o}{o_0, o_1, o_2} \mathbb{P}\big(^{16}\text{O}\big)^{o_0} \mathbb{P}\big(^{17}\text{O}\big)^{o_1} \mathbb{P}\big(^{18}\text{O}\big)^{o_2}
\binom{o}{o_0, o_1, o_2} \mathbb{P}\big(^{32}\text{S}\big)^{s_0} \mathbb{P}\big(^{33}\text{S}\big)^{s_1} \mathbb{P}\big(^{34}\text{S}\big)^{s_2} \mathbb{P}\big(^{36}\text{S}\big)^{s_3}. 
\end{gather*}
Aby znaleźć masę tego izotopologa, należy natomiast przemnożyć liczby atomów w danych wariantach izotopowych przez ich masy, a następnie zsumować,
$$m_{\tt izo} = m(^{12}\text{C}) c_0 + m(^{13}\text{C}) c_1 + \dots + m(^{36}\text{S}) s_3.$$
Ciąg par $(p_{\tt izo}, m_{\tt izo})$ wyznaczony przez cząsteczkę \ce{C_c H_h N_n O_o S_s} przyjęto nazywać \textit{subtelną strukturą izotopową}. 
Tak określony rozkład znajduje bezpośrednie zastosowanie w analizie widma masowego.

\subsection*{\hfil{\Futura Obliczenia izotopowe}\hfil}

W pierwszym rozdziale roprawy doktorskiej przedstawię algorytm umożliwiający szybkie generowanie elementów subtelnej struktury izotopowej.

Załóżmy, że $\mathcal{E}$ to zbiór pierwiastków tworzących daną cząsteczkę.
Niechaj ustalony pierwiastek $e \in \mathcal{E}$ ma $i_e$ wariantów izotopowych oraz niech będzie reprezentowany w cząsteczce przez $n_e$ atomów.
Wtedy, zwłaszcza dla dużych cząsteczek, dla których $n_e \gg 0$, wyliczanie pełnego zbioru izotopologów jest pozbawione sensu, jako że tworzy go $\prod_{e \in \mathcal{E}} \binom{n_e + i_e - 1}{n_e}$ elementów.
Stosując wzór Stirlinga, łatwo zauważyć, że jest to wyrażenie rzędu $\mathcal{O}( \prod_{e \in \mathcal{E}} n_e^{i_e-1} )$.
Przykładowo, na subtelną strukturę izotopową ludzkiej insuliny składa się ponad $10^{14}$ różnych izotopologów. 
Tymczasem, raptem 1~716 najbardziej prawdopodobnych izotopologów łącznie reprezentuje niewiele ponad 99\% masy prawdopodobieństwa, 5~403 konfiguracje reprezentują 99.9\% masy prawdopodobieństwa, a 13~101 -- 99.99\%.
Tym samym, warto porzucić zagadnienie wyliczania pełnej struktury na rzecz wyliczania zbioru znacznie mniej licznego, którego zawartość potrafilibyśmy bezpośrednio kontrolować. 

\begin{definicja}
    Optymalny $P$-zbiór to najmniejszy zbiór najbardziej prawdopodobnych izotopologów, których sumaryczne prawdopodobieństwo przekracza $P \in [0,1]$. W przypadku większej liczby takich zbiorów optymalnym $P$-zbiorem nazywamy jakiegokolwiek reprezentanta tej klasy zbiorów izotopologów.
\end{definicja}

W rozdziale pierwszym mojej rozprawy opisuję algorytm służący generowaniu $P$-zbiorów, o nazwie {\tt IsoSpec}.
Zaproponowany algorytm wykorzystuje dwie własności rozkładu wielomianowego: koncentrację miary\citep{Talagrand1996} oraz jednomodalność \citep{Finucan1964}. 
Koncentracja miary to zjawisko polegające na tym, że mała liczba konfiguracji jest nośnikiem sporej miary prawdopodobieństwa.
Jednomodalność, wysłowiona w danym kontekście jako spójność zbioru lokalnych maksimów, umożliwia odwiedzanie jego konfiguracji w malejącej kolejności względem prawdopodobieństwa.
Szczęśliwie, obie powyższe własności zachowane są również przy rozpatrywaniu produktów takich rozkładów, a więc znajdują bezpośrednie zastosowanie w wyliczaniu elementów subtelnej struktury izotopowej.

\begin{figure}[t]
    \centering
    \includegraphics[width=\linewidth]{cltSimplex.png}
    \caption{Jakość przybliżeń gaussowskich optymalnego $P$-zbioru dla cząsteczki zbudowanej z jednego typu pierwiastka (dla uproszczenia) z trzema różnymi izotopami. 
    \textit{Centralne twierdzenie graniczne} orzeka, że rozkład wielomianowy dobrze przybliża się normalnym, co sugeruje wykorzystanie elipsoid jako ciągłego odpowiednika optymalnego $P$-zbioru.
    Środek elipsoidy zaczepiony jest w średniej rozkładu wielomianowego. 
    Jej kształt wyznaczony jest przez macierz kowariancji.
    Powyższe sympleksy zostały znormalizowane przez podzielenie ich przez liczbę atomów w subtancji, wynoszacych $n \in \{25, 50, 75, 100\}$. 
    Pole tych elipsy maleje z $n$.
    Przybliżenie rozkładem normalnym sugeruje, że wielkość elips zachowuje się jak pierwiastek z liczby atomów. 
    Przy powyższej normalizacji oznacza to, że elipsy na rysunkach powinny maleć w tempie $\frac{\sqrt{n}}{n} = n^{-1/2}$.
    }\label{fig::clt on simplex}
\end{figure}

W rozdziale tym zostaje podany dowód tego, że złożoność czasowa wygenerowania elementów optymalnego $P$-zbioru jest liniowa względem ich liczby.
Tym samym jest to algorytm optymalny.
W celu określenia złożoności czasowej wykorzystane zostaje \textit{centralne twierdzenie graniczne} w celu przybliżenia liczby elementów optymalnego $P$-zbioru.
Ideę tą obrazuje Rysunek~\ref{fig::clt on simplex}.
Co więcej, pokazujemy, że liczbę izotopologów można bezpośrednio przybliżyć przez
\begin{equation*}
    M = \frac{q_{\chi^2(k)}(P)^\frac{k}{2}}{C} \frac{\pi^{k/2}}{\Gamma(k/2+1)}\sqrt{ \prod_{e\in\mathcal{E}} \Big(n_e^{i_e - 1} \prod_{j=0}^{i_e-1} \tilde{d}_{ej} } \Big).
\end{equation*}
Wynika stąd, że dla ustalonego $P$, liczba elementów optymalnych $P$-zbiorów zachowuje się w przybliżeniu jak $\mathcal{O}(\sqrt{\prod_{e\in\mathcal{E}}n_e^{i_e - 1}})$.
Stopień wielomianu jest zatem dwa razy niższy.
Co więcej, zaimplementowany algorytm jest szybszy od innych istniejących rozwiązań, umożliwiając nawet obliczenia dla substancji tak dużych, jak tytyna\citep{opitz2003damped}, \ce{C169719H270464N45688O52237S911}\footnote{Obliczenia na tak dużych białkach oczywiście nie mają sensu (izotopologi zajmują gigabajty pamięci). Chodzi tylko o zobrazowanie możliwości algorytmu.}.

Algorytm jest stale rozwijany, a rozwinięte przy jego tworzeniu pomysły znalazły już zastosowanie przy rozwiązywaniu problemów związanych z symulacjami statystycznymi\citep{startek2016asymptotically} oraz w statystyce.

\subsection*{\hfil{\Futura Dekonwolucja widm masowych i statystyka jonów}\hfil}

Subtelna struktura izotopowa opisuje warianty tylko jednej cząsteczki chemicznej.
Natomiast wiadomo, że widmo masowe powstaje w wyniku zarejestrowania ruchu bardzo wielu cząsteczek.
Niemniej, jeśli założy się, że trajektorie jonów są od siebie niezależne oraz jest ich dużo, to widmo jednej substancji teoretycznie powinno być proporcjonalne do pików rozkładu izotopowego.
Fakt ten jest skwapliwie wykorzystywany przez wiele algorytmów, których zadaniem jest deizotopizacja sygnału, czyli powiązanie wielu pików w jedną wspólną strukturę.
Taka uśredniona obwiednia jest również wykorzystywana do dekonwolucji sygnału pochodzącego z wielu (znanych) źródeł.
W tym celu można wykorzystać metody regresji nieujemnej\citep{slawski2012isotope}, które i my wykorzystujemy w projekcie {\tt MassTodon}, omawianym dalej.

A jednak głębsze spojrzenie na problem, które zakłada zmienność na poziomie pojawiających się cząsteczek -- czyli statystyka jonów -- dostarczyć może ciekawych informacji na temat ich liczby.
Teoria spektrometrii podpowiada, że liczba jonów, które dolatują do detektora można opisać rozkładem Poissona~\citep{Ipsen2012,Ipsen2015}.
Prosty argument teoretyczny uprawomocnia to stwierdzenie: jeśli bowiem założymy, że trajektorie jonów w spektrometrze są niezależne, oraz z pewnym ustalonym prawdopodobieństwem jony dolatują do detektora, to liczbę zarejestrowanych jonów opisuje rozkład dwumianowy.
Jeśli prawdopodobieństwo zarejestrowania jonu jest małe w porównaniu do ich łącznej liczby, to rozkład ten jest dobrze przybliżony przez rozkład Poissona, który z powyższego powodu nazywany bywa rozkładem zdarzeń rzadkich.

W rozdziale czwartym opiszę próby połączenia idei subtelnej struktury izotopowej z poissonowską liczbą jonów.
Zaprezentowane tam narzędzie, które nazywamy {\tt MassOnem}, umożliwia
\begin{enumerate}
    \item dekonwolucję sygnału
    \item estymowanie stałej proporcjonalności w liniowej zależności pomiędzy liczbą jonów, a zaobserwowaną intensywnością piku.
\end{enumerate}
Potrzeba dekonwolucji sygnału wynika z ograniczonej rozdzielczości spektrometru.
Cząsteczki o bardzo podobnym stosunku masy do ładunku, często reprezentowane są przez instrument pod postacią jednego piku.
Problem ten jest zobrazowany na Rysunku~\ref{fig::ball spectrum} na przykładzie dwóch typów cząsteczek, których obwiednie się nakładają. 
\begin{figure}[th]
    \centering
    \includegraphics[width=.8\linewidth]{ball_spectrum_pl}
    \caption{Schematyczna reprezentacja splotu dwóch struktur izotopowych.
    Każda kulka reprezentuje jeden typ cząsteczki, typu {\Futura A} lub {\Futura B}. 
    Izotopologi cząsteczek wpadają do różnych przedziałów masowych.
    Powyższy wzorzec widm przesuniętych dokładnie o masę jednego daltona można spotkać np. gdy jedna z cząsteczek posiada dodatkowy wodór.}\label{fig::ball spectrum}
\end{figure}

Drugi problem wynika stąd, że wykorzystywane zwykle detektory rejestrują sygnał tylko proporcjonalny do liczby jonów, w myśl równania $I = C \times N$, gdzie $N$ to liczba jonów, a $I$ to obserwowana intensywność.
Poznanie faktycznej liczby jonów zaobserwowanych w widmie masowym jest bardzo istotne, albowiem od liczby tej zależą oszacowania odchyleń standardowych wielu statystyk będących funkcjami słupków widma masowego.
Problem pojawia się przy próbie oszacowania zmienności oszacowań prawdopodobieństw reakcji chemicznych, które mogły przyczynić się do kształtu zaobserwowanego widma. Zagadnienie to opisuję w rozdziałach drugim i trzecim rozprawy doktorskiej.
\MassON atakuje oba te problemy jednocześnie, posługując się metodami statystyki bayesowskiej.
Wykorzystuje on metodę \textit{dokładania danych}\footnote{Po angielsku, \textit{data augmentation}. Ponieważ nie istnieje utarty sposób tłumaczenia tego terminu, proponuję krótkie tłumaczenie zachowujące sens metody.} polegającą, w uproszczeniu, na dolosowywaniu nieobserwowanych stanów modelu.
Same obliczenia przeprowadzone są z wykorzystaniem algorytmu Gibbsa\citep{geman1984stochastic}.

W proponowanym podejściu każde źródło jonów traktujemy jako proces poissonowski określony na półprostej $[0,\infty)$ możliwych wartości mass do ładunku.
Procesy są warunkowo niezależne przy znanych łącznych liczebnościach jonów.
Poszczególny proces opisuje miara intensywności, będąca mieszanka rozkładów normalnych.
Rozkłady te scentrowane są w stosunkach mas do ładunku wyznaczonych przez model subtelnej struktury izotopowej oraz wymnożone przez łączną średnią liczbę jonów $\Lambda_m$, 
\begin{equation}
    \mu_m (A | \Lambda_m) = \Lambda_m \sum_{w = 1}^W p_{mw} \mathtt{G}(A|{\tt MZ}_{mw}).
\end{equation}
Powyżej, $p_{mw}$ to kolejne prawdopodobieństwa izotopologów, a ${\tt MZ}_{mw}$ to ich masy. Natomiast $A$ to pewien przedział w domenie mas.

\subsection*{\hfil{\Futura Rozpad biomolekuł indukowany transferem elektronu}\hfil}

Spektrometr mas nie jest w stanie rozróżnić substancji, które składają się z identycznych liczb atomów różnych pierwiastków.
W szczególności nie byłby on w stanie rozróżnić dwóch takich białek różniących się jedynie pozycją modyfikacji posttranslacyjnej jednego z aminokwasów.
Modyfikacje te mogą wpłynąć na różnice w ich konformacji przestrzennej, a tym samym -- na ich biologiczną rolę.
Sposobu na poradzenie sobie z tym problemem dostarcza fragmentacja jonów.
Można jej dokonać bądź to poza instrumentem, w wyniku indukowanej proteolizy.
Można też dokonać jej w instrumencie, powodując rozpad jonów w wyniku kolizji z atomami neutralnego gazu (Collisional Induced Dissociation - CID), lub dzięki transferowi elektronu (Electron Transfer Dissociation - ETD).
ETD jest subtelniejszą techniką, która polega na spotkaniach kationów z próbki z anionami-rodnikami.
W wyniku takiego spotkania może dojść do czterech możliwych wyników:
\begin{itemize}
    \item przekazaniu elektronu i dalszego rozpadu kationu na dwa fragmenty, oznaczane jako $c$ i $z$. Jest to właściwe ETD.
    \item przekazaniu elektronu niewywołującego rozpadu -- ETnoD
    \item ETD z następnym transferem wodoru z $c$ fragmentu na $z$ fragment -- HTR
    \item wyrwaniu jednego z protonów-ładunków kationu przez anion -- PTR
\end{itemize}
Tabela~\ref{chemical_reactions} uszczegóławia powyższe reakcje.
Reakcje te mogą zachodzić wielokrotnie na poszczególnych kationach, z wyjątkiem ETD i HTR.
Uproszczenie to jest akceptowalne bo zwykle nie obserwuje się wewnętrznych fragmentów łańcuchów aminokwasowych w widmach masowych.
\begin{table*}[b]
\centering
\begin{tabular}{rlcl}
    \textbf{PTR}    &\ce{[M + nH]^{n+} + A^{.-}}    &\ce{->}& \ce{[M + (n-1) H]^{(n-1)+} + AH}  \\
    \textbf{ETnoD}  &\ce{[M + nH]^{n+} + A^{.-}}    &\ce{->}& \ce{[M + nH]^{(n-1)+.} + A}      \\
    \textbf{ETD}    &\ce{[M + nH]^{n+} + A^{.-}}    &\ce{->}& \ce{[c + xH]^{x+} + [z + (n - x)H]^{(n-x-1)+.} + A}\\
    \textbf{HTR}    &\ce{[c + xH]^{x+}}             &\ce{->}& \ce{[c + (x - 1)H]^{x+}}\\
                    &\ce{[z + (n - x)H]^{(n-x-1)+}}     &\ce{->}& \ce{[z + (n - x + 1)H]^{(n-x-1)+}}
\end{tabular}
\caption{Wzory chemiczne rozpatrywanych reakcji. \ce{M} oznacza tu prekursora, czyli jon wyodrębniony w pierwszej fazie eksperymentu tandemowego. Reakcja HTR może nastąpić dopiero po dopełnieniu się reackji ETD, ale na potrzeby modelowania łatwiej nam uznać ją za kolejną reakcję.}\label{chemical_reactions}
\end{table*}
Aby je lepiej zrozumieć, stworzyliśmy algorytm -- ${\tt MassTodon}$ -- który zostaje przedstawiony w drugim rozdziale rozprawy.
Algorytm ten jest wielofazowy,
\begin{enumerate}
    \item generuje formuły chemiczne możliwych produktów reakcji
    \item namierza ślady produktów w widmie \label{point::investigation}
    \item rozplata struktury izotopowe różnych produktów za pomocą algorytmu programowania kwadratowego \label{point::deconvolution}
    \item paruje pasujące $c$ i $z$ jony w celu oszacowania prawdopodobieństw fragmentacji oraz ilorazu szans reakcji ETnoD i PTR
\end{enumerate}
W celu rozwiązania zadań~\ref{point::investigation} i \ref{point::deconvolution}, {\tt MassTodon} wykorzystuje subtelne struktury izotopowe wyliczone przez algorytm \IsoSpeC.
Wykorzystanie \textit{grafu dekonwolucji} (Rysunek~\ref{fig::deconvolution_principles}) umożliwia efektywne, równoległe przeprowadzenie obliczeń.

\begin{figure}[t]
    \centering
    \includegraphics[width=.7\linewidth]{deconvolution_graph6}
    \caption{Spójna składowa \textit{grafu dekonwolucji}. Dwie substancje, $M_A$ and $M_B$, potencjalnie tworzą widmo eksperymentalne, na które składają pionowe różowe słupki. 
    Obie substancje mają trzy typy izotopologów. 
    Proporcje pomiędzy ich teoretycznymi prawdopodobieństwami, $p_{A_0} \div p_{A_1} \div p_{A_2}$ and $p_{B_0} \div p_{B_1} \div p_{B_2}$, wykorzystane są do rozplecenia sygnału na jego dwa źródła.
    Zadaniem stawianym przed {\tt MassTodonem} jest oszacowanie łącznych intensywności substancji $M_A$ and $M_B$, które to powyżej są oznaczone jako $\alpha_A$ i $\alpha_B$.}\label{fig::deconvolution_principles}
\end{figure}

{\tt MassTodon} oszacowuje łączne intensywności substratów i produktów badanych reakcji, jak i prawdopodobieństwa reakcji oraz fragmentacji różnych aminokwasów.
Znamienicie upraszcza on porównawczą analizę widm, zarówno z tego samego instrumentu, pozyskanych przy różnych jego ustawieniach, jak i wyniki uzyskane z różnych instrumentów.
W szczególności {\tt MassTodon} został wykorzystany do pomiaru ilorazu szans reakcji ETnoD i PTR, który to dobrze koreluje ze wielkością przekroju kolizyjego\footnote{Z ang. \textit{collisional cross-section}.}, czyli pomiarów struktury trzeciorzędowej białek\citep{lermyte2017conformational}.

\subsection*{\hfil{\Futura Studium kinetyki reakcji}\hfil}

W rozdziale trzecim patrzymy na dostarczane przez {\tt MassTodona} oszacowania łącznej intensywności fragmentów przez pryzmat stałych kinetyki reakcji.
Matematyczna teoria kinetyki reakcji jest bogatą, dobrze rozwiniętą teorią.
W szczególności, interesującym zagadnieniem jest zaadaptowanie podejścia opartego na dynamicznych stochastycznych sieciach Petriego do opisu widm masowych, na wzór wcześniejszych prac w tym duchu\citep{Gambin2010}.
Rysunek~\ref{img::petrinet} zawiera uproszczony schemat zasad stojących u zarania wyprowadzonych przez nas równań reakcji chemicznych.
\begin{figure}[t]
    \begin{subfigure}[b]{.65\linewidth}    
    \centering
    \includegraphics[width=\textwidth]{reaction_graph}
    \caption{Sieć Petriego}
    \end{subfigure}
    \begin{subfigure}[b]{.33\linewidth}    
    \centering
    \includegraphics[width=\textwidth]{reaction2_pl}
    \caption{Reakcja ETD}
    \end{subfigure}
    \caption{uproszczony schemat reakcji ETD w postaci sieci Petriego.
    (a) fragment grafu reakcji dla potrójnie naładowanego prekursora. 
    Cząsteczki chemiczne zaznaczone są na pomarańczowo, a reakcje na niebiesko.
    Różowa czaszka reprezentuje cmentarz.
    Graf to plansza, na której rozmieszczone są czerwone kulki reprezentujące poszczególne jony.
    Część (b) pokazuje zachowanie kulek podczas reakcji ETD, w czasie to której jeden jon substrat zamienia się w dwa różne jony produkty.
    Każda reakcja konsumuje jeden jon, przy czym ETD i HTR tworzą dwa, a PTR i ETnoD generują po jednym jonie-produkcie.
    Jony, które w wyniku reakcji tracą cały ładunek, trafiają do cmentarza.}\label{img::petrinet}
\end{figure}

Model jest opisany zestawem parametrów wpływających na intensywności przejścia procesu Markowa z czasem ciągłym i dyskretną przestrzenią stanów.
Intensywności te powiązane są w naturalny sposób z reakcjami uwzględnianym w modelu. 
Relatywnie prosta struktura sieci Petriego, w szczególności -- jej acykliczność, upraszczają dynamikę procesu, domykając równania opisujące uśrednioną dynamikę reakcji.
Uzyskaliśmy rekurencyjne równania, którymi można się posłużyć przy wyliczaniu średnich liczebności jonów w poszczególnych stanach dla ustalonego chwili procesu.
Zmiany w parametrach skutkują różnymi zajętościami.
W celu zminimalizowania dystansu pomiędzy zaobserwowanymi zajętościami, a przewidywaniami procesu, wykorzystujemy bezgradientowy algorytm przeszukujący przestrzeń parametrów. 

Powstałe narzędzie, zwane {\tt ETDetective}, jest w pełni kompatybilne z algorytmem {\tt MassTodon}.
Przygotowywany jest serwis internetowy umożliwiający zdalne wywołanie obu programów.

\ornamentheader{spis publikacji w dziedzienie spektrometrii mas}

\begin{itemize}
  \item Lermyte, F., Łącki, M. K., Valkenborg, D., Gambin, A., \& Sobott, F. (2017). Conformational space and stability of ETD charge reduction products of ubiquitin. Journal of The American Society for Mass Spectrometry, 28(1), 69-76.
  \item  Lermyte, F., Łącki, M. K., Valkenborg, D., Baggerman, G., Gambin, A., \& Sobott, F. (2015). Understanding reaction pathways in top-down ETD by dissecting isotope distributions: A mammoth task. International Journal of Mass Spectrometry, 390, 146-154. 
  \item Ciach, M. A., Łącki, M. K., Miasojedow, B., Lermyte, F., Valkenborg, D., Sobott, F., \& Gambin, A. (2017, May). Estimation of Rates of Reactions Triggered by Electron Transfer in Top-Down Mass Spectrometry. In International Symposium on Bioinformatics Research and Applications (pp. 96-107). Springer, Cham.
  \item Łącki, M. K., Lermyte, F., Miasojedow, B., Startek, M., Sobott, F., ... \& Gambin, A. (2017). Assigning peaks and modeling ETD in top-down mass spectrometry. arXiv preprint arXiv:1708.00234.
  \item Łącki, M. K., Startek, M., Valkenborg, D., \& Gambin, A. (2017). IsoSpec: Hyperfast Fine Structure Calculator. Analytical Chemistry, 89(6), 3272-3277.
\end{itemize}

\ornamentheader{pozostałe publikacje}
\begin{itemize}
    \item Bielczyński, L.W., Łącki, M.K., Hoefnagels, I., Gambin, A., \& Croce, R. (2017). Leaf and plant age affects photosynthetic performance and photoprotective capacity. Warunkowo zaakceptowane do druku w Plant Physiology.
\end{itemize}

\bibliography{bib/my_works,bib/algorithmics,bib/probability,bib/frederik,bib/spectrometry,bib/statistics,bib/ciach}

\end{document}
